/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.index.persistent;

import java.io.IOException;
import java.io.ObjectStreamException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.BucketStorageException;
import messif.buckets.storage.LongAddress;
import messif.objects.nio.BinaryInput;
import messif.objects.nio.BinaryOutput;
import messif.objects.nio.BinarySerializable;
import messif.objects.nio.BinarySerializator;
import messif.objects.util.AbstractObjectIterator;
import messif.operations.QueryOperation;
import mindex.MetricIndex;
import mindex.navigation.SplitConfiguration;
import mindex.navigation.VoronoiInternalCell;
import mindex.navigation.VoronoiLeafCell;
import pppcodes.PPPCodeIndexFile;
import pppcodes.index.PPPCodeLeafCell;
import static pppcodes.index.PPPCodeLeafCell.bitsToBytes;
import pppcodes.index.PPPCodeObject;

/**
 * A leaf cell that does not store the actual data by serialization but in a separate binary storage.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeLeafCellFile extends PPPCodeLeafCell {

    /** Class id for serialization. */
    private static final long serialVersionUID = 412501L;

    /** Link to specific storage and address within this storage where the leaf data is stored. */
    protected LongAddress<PPPCodeLeafData> fileAddress = null;
    
    /**
     * Constructor given all parameters mandatory for the parent class. The storage and the covered interval are set to null.
     * @param mIndex M-Index logic
     * @param parentNode parent node of this node
     */
    public PPPCodeLeafCellFile(MetricIndex mIndex, VoronoiInternalCell parentNode) {
        super(mIndex, parentNode);
    }

    /**
     * Constructor that copies inherited fields from given cell and uses specified data and file address.
     * @param copyFrom a leaf node to copy all inherited fields from.
     * @param positionBits current length of the data in bits
     * @param data byte array with the PPP-Codes + IDs
     * @param fileAddress address in the disk storage
     */
    public PPPCodeLeafCellFile(VoronoiLeafCell copyFrom, int positionBits, byte [] data, LongAddress<PPPCodeLeafData> fileAddress) {
        super(copyFrom, positionBits, data);
        this.fileAddress = fileAddress;
    }
    
    /**
     * Instead of the standard {@link PPPCodeLeafCell} serialization, the data is not written.
     * @return
     * @throws ObjectStreamException 
     */
    protected Object writeReplace() throws ObjectStreamException {
        return new PPPCodeLeafCellNoData(this);
    }
    
    /**
     * A stub class to be created only when this leaf is to be serialized; the serialization 
     *  DOES NOT store the data {@link #data} itself but only other node metadata.
     */
    protected static class PPPCodeLeafCellNoData extends VoronoiLeafCell<PPPCodeObject> {

        /** Class id for serialization. */
        private static final long serialVersionUID = 412601L;
        
        protected LongAddress<PPPCodeLeafData> fileAddress = null;
        
        public PPPCodeLeafCellNoData(PPPCodeLeafCellFile copyFrom) throws IllegalArgumentException {
            super(copyFrom);
            this.fileAddress = copyFrom.fileAddress;
            if (fileAddress == null) {
                throw new IllegalArgumentException("Serializing Voronoi leaf node via " + this.getClass() + " while the storage (file address) is not set - data would be LOST");
            }
        }
        
        protected Object readResolve() throws ObjectStreamException {
            PPPCodeLeafData dataObject;
            try {
                dataObject = fileAddress.read();
            } catch (BucketStorageException ex) {
                throw new IllegalArgumentException(ex);
            }
            return new PPPCodeLeafCellFile(this, dataObject.positionBits, dataObject.data, fileAddress);
        }
        
        @Override
        public SplitConfiguration insertObjects(List<PPPCodeObject> objects) throws AlgorithmMethodException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        @Override
        public Collection<PPPCodeObject> deleteObjects(List<PPPCodeObject> objects, int deleteLimit, boolean checkLocator) throws AlgorithmMethodException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        @Override
        public boolean split(SplitConfiguration splitConfiguration) throws AlgorithmMethodException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        @Override
        public int processOperation(QueryOperation operation) throws AlgorithmMethodException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        @Override
        public AbstractObjectIterator<PPPCodeObject> getAllObjects() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        @Override
        public void clearData() throws AlgorithmMethodException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        @Override
        public void calculateTreeSize(Map<Short, AtomicInteger> intCellNumbers, Map<Short, AtomicLong> branchingSums, AtomicInteger leafCellNumber, AtomicLong dataSizeBytes, AtomicLong leafLevelSum) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    /**
     * This class serves for binary (de)serialization of the leaf data to binary storage file.
     * It stores the data into chunks of fixed size(s) starting at {@link #INIT_CHUNK_SIZE_BYTES} 
     * and, if necessary, growing by ratio {@link #CHUNK_GROW_RATIO}. Smaller data ([] byte) are padded
     * by zeros.
     */
    public static class PPPCodeLeafData implements BinarySerializable {

        protected static final int INIT_CHUNK_SIZE_BYTES = 256;
        protected static final float CHUNK_GROW_RATIO = 1.5f;
        
        protected final int positionBits;
        protected final byte [] data;
        
        protected transient int binarySize;
        
        /**
         * Creates new object of this type given data to be serialized.
         * @param positionBits actual size of the data in bits
         * @param data byte array carrying the PPP-Code leaf data
         */
        public PPPCodeLeafData(int positionBits, byte [] data) {
            this.data = data;
            this.positionBits = positionBits;
            this.binarySize = INIT_CHUNK_SIZE_BYTES;
            while (bitsToBytes(positionBits) + 8 > binarySize) {
                binarySize = (int) (binarySize * CHUNK_GROW_RATIO);
            }
        }

        /**
         * Constructor called during the binary deserialization from the file.
         * @param input
         * @param serializator
         * @throws IOException 
         */
        public PPPCodeLeafData(BinaryInput input, BinarySerializator serializator) throws IOException {
            this.positionBits = serializator.readInt(input);
            this.data = Arrays.copyOf(serializator.readByteArray(input), bitsToBytes(positionBits));
        }
        
        @Override
        public int getBinarySize(BinarySerializator serializator) {
            return binarySize;
        }

        @Override
        public int binarySerialize(BinaryOutput output, BinarySerializator serializator) throws IOException {
            return serializator.write(output, positionBits) + serializator.write(output, Arrays.copyOf(data, binarySize - 8));
        }
    }
    
    /**
     * This method stores the actual leaf binary data into the storage (if specified). Previously 
     *  occupied data chunk is rewritten, if the data did not grow too much.
     * @return
     * @throws BucketStorageException 
     */
    protected void writeToStorage() throws BucketStorageException {
        if (fileAddress == null && ((PPPCodeIndexFile) mIndex).getLeafStorage() == null) {
            return;
        }
        PPPCodeLeafData leafData = new PPPCodeLeafData(getOccupationInBits(), data);
        if (fileAddress == null) {
            fileAddress = ((PPPCodeIndexFile) mIndex).getLeafStorage().store(leafData);            
            return;
        }
        try {
            fileAddress.rewrite(leafData);
        } catch (IllegalArgumentException ex) {
            // this happens if the data grew over the previous chunk size
            fileAddress.remove();
            fileAddress = ((PPPCodeIndexFile) mIndex).getLeafStorage().store(leafData);
        }
    }
    
    @Override
    public synchronized SplitConfiguration insertObjects(List<PPPCodeObject> objects) throws AlgorithmMethodException {
        SplitConfiguration split = super.insertObjects(objects);
        if (split == null) {
            try {
                writeToStorage();
            } catch (BucketStorageException ex) {
                throw new AlgorithmMethodException("failed to write leaf data to storage: " + ex.getLocalizedMessage());
            }
        }
        return split;
    }
    
    @Override
    public synchronized boolean consolidateData(byte [] temporary) {
        boolean retVal = super.consolidateData(temporary);
        try {
            if (retVal) {
                writeToStorage();
            }
        } catch (BucketStorageException ex) {
            Logger.getLogger(PPPCodeLeafCellFile.class.getName()).log(Level.SEVERE, null, ex);            
        }
        return retVal;
    }

    @Override
    protected void invalidateThisNode(VoronoiInternalCell<PPPCodeObject> newInternalSubstitute) {
        super.invalidateThisNode(newInternalSubstitute);
        if (fileAddress != null) {
            try {
                fileAddress.remove();
            } catch (BucketStorageException | UnsupportedOperationException ex) {
                Logger.getLogger(PPPCodeLeafCellFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }    

    // ******************    Testing methods    ********************* //
    
    /**
     * This is just a testing method that should not be called under normal circumstances.
     * Reads all the data from PPP-Code file storage into the memory.
     * @throws BucketStorageException if the reading fails
     */
    public void reloadDataTest() throws BucketStorageException {
        if (fileAddress != null) {
            if (writeBufferBits != null || deletedIndexes != null) {
                throw new IllegalStateException(" ERROR: writeBuffer or deleted indexes are not null: " + writeBufferBits + ", " + deletedIndexes);
            }
            PPPCodeLeafData loaded = fileAddress.read();
            this.data = loaded.data;
            this.positionBits = loaded.positionBits;
        }
    }
}
