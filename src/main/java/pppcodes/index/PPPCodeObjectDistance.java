/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.index;

import java.nio.BufferUnderflowException;
import mindex.processors.SinglePPPRankable;
import mindex.distance.PartialQueryPPPDistanceCalculator;
import mindex.distance.QueryPPPDistanceCalculator;

/**
 * 
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeObjectDistance extends PPPCodeObject implements RankableLocators {

    /** Serialization UID. */
    private static final long serialVersionUID = 963101L;
    
    /** Query-object distance or any other float value to sort the objects */
    private final float distance;
    
    
    // ***************** Constructors *****************//

    /**
     * Creates new Encrypted object from given ID and PPP array.
     * @param calculator encapsulation of a query object that can calculate single distance from the created PPPCode object
     */
    public PPPCodeObjectDistance(int locator, short [] ppp, QueryPPPDistanceCalculator calculator) {
        super(locator, ppp);
        this.distance = calculator.getQueryDistance(ppp);
    }    
 
    /**
     * Reads and create a PPP code object from specified byte buffer creating also the single PPP-Code distance using
     *  given distance calculator.
     * @param input byte buffer to read the data from (with internal position)
     * @param tailArrayLengthToRead length of the PPP array
     * @param calculator encapsulation of a query object that can calculate single distance from the created PPPCode object
     * @param pppUpper prefix of the single PPP code to be used for creating the distance
     * @throws BufferUnderflowException if the buffer does not have sufficient data to read the 
     */
    public PPPCodeObjectDistance(ByteBufferBits input, PPPCodeReadWriter pppCodeReader, int tailArrayLengthToRead, 
            QueryPPPDistanceCalculator calculator, short [] pppUpper) throws BufferUnderflowException {
        super(input, pppCodeReader, tailArrayLengthToRead, pppUpper);
        this.distance = calculator.getQueryDistance(ppp);
    }
    
    public PPPCodeObjectDistance(ByteBufferBits input, PPPCodeReadWriter pppCodeReader, int tailArrayLengthToRead, 
            PartialQueryPPPDistanceCalculator particalCalculator, int levelToStartFrom, float topDistance) throws BufferUnderflowException {
        super(input, pppCodeReader, tailArrayLengthToRead);
        this.distance = topDistance + particalCalculator.getPartialQueryDistance(ppp, levelToStartFrom);
    }
    
    // *******************    Comparable     *********************** //
    
    @Override
    public float getQueryDistance() {
        return distance;
    }

    @Override
    public RankableLocators readLocators() {
        return this;
    }    
}
