/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.index;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.ObjectProvider;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.util.ObjectProvidersIterator;
import mindex.MetricIndex;
import mindex.navigation.VoronoiCell;
import mindex.navigation.VoronoiInternalCell;
import mindex.distance.PartialQueryPPPDistanceCalculator;
import mindex.distance.QueryPPPDistanceCalculator;

/**
 * Encapsulates information about a super-cluster in the M-Index dynamic cluster hierarchy.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeInternalCell extends VoronoiInternalCell {

    /** Class id for serialization */
    private static final long serialVersionUID = 412201L;
    
    /**
     * Create new object initializing cluster number, level and parentNode. The created node is empty - no child nodes are created!
     * @param mIndex M-Index logic
     * @param parentNode parent node of this node
     * @param pivotCombination the pivot combination corresponding to this cluster, e.g. [3,2,4] for cluster C_{3,2,4}
     * @throws AlgorithmMethodException if the cluster storage was not created successfully
     */
    public PPPCodeInternalCell(MetricIndex mIndex, VoronoiInternalCell parentNode, short[] pivotCombination) throws AlgorithmMethodException {
        super(mIndex, parentNode, pivotCombination);
    }

    /**
     * The serialization of internal PPP-Code cells now locks the whole subtree. TODO: maybe remove
     * @param out
     * @throws IOException 
     */
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        writeLock();
        try {
            out.defaultWriteObject();
        } finally {
            writeUnLock();
        }
    }    

    // ************************   Sub-level manipulation  *********************************** //
    
    /**
     * Creates a specific child according to the type of this internal cell (data/nodata etc.)
     * @param internalNode if to crate an internal or leaf cell
     * @return newly created cell
     * @throws messif.algorithms.AlgorithmMethodException if anything goes wrong
     */
    @Override
    public VoronoiCell createSpecificChildCell(boolean internalNode, short[] newCombination) throws AlgorithmMethodException {
        return internalNode ? new PPPCodeInternalCell(mIndex, this, newCombination) :
                new PPPCodeLeafCell(mIndex, this);        
    }    
    
    public AbstractObjectIterator<? extends PPPCodeObject> getAllObjects(final QueryPPPDistanceCalculator calculator, final float thisCellDistance) {
        Collection<ObjectProvider<? extends PPPCodeObject>> subIterators = new ArrayList<>();
        for (Iterator<Map.Entry<Short, VoronoiCell>> it = getChildNodes(); it.hasNext();) {
            Map.Entry<Short, VoronoiCell> childCell = it.next();
            float childDistance = (calculator instanceof PartialQueryPPPDistanceCalculator) ? 
                        (((PartialQueryPPPDistanceCalculator) calculator).getPartialQueryDistance(childCell.getKey(), getLevel() + 1) + thisCellDistance) : thisCellDistance;
            if (childCell.getValue() instanceof PPPCodeInternalCell) {
                subIterators.add(((PPPCodeInternalCell) childCell.getValue()).getAllObjects(calculator, childDistance));
            } else {
                subIterators.add(((PPPCodeLeafCell) childCell.getValue()).getAllObjects(calculator, childCell.getValue().getPppForReading(childCell.getKey()), childDistance));
            }
        }
        return new ObjectProvidersIterator<>(subIterators);
    }
    
    // ************************    Data manipulation     *************************** //
    
    public void consolidateData(byte [] temporary) {
        readLock();
        try {
            for (Iterator<Map.Entry<Short, VoronoiCell>> childNodes = getChildNodes(); childNodes.hasNext(); ) {
                VoronoiCell child = childNodes.next().getValue();
                if (child instanceof PPPCodeInternalCell) {
                    ((PPPCodeInternalCell) child).consolidateData(temporary);
                } else {
                    ((PPPCodeLeafCell) child).consolidateData(temporary);
                }
            }
        } finally {
            readUnLock();
        }
    }

    // **********************    For testing purposes    ********************* //
    
    public void findObject(int id) {
        for (Iterator<Map.Entry<Short, VoronoiCell>> childNodes = getChildNodes(); childNodes.hasNext(); ) {
            VoronoiCell child = childNodes.next().getValue();
            if (child instanceof PPPCodeInternalCell) {
                ((PPPCodeInternalCell) child).findObject(id);
            } else {
                ((PPPCodeLeafCell) child).findObject(id);
            }
        }
    }
    
}
