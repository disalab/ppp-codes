/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.ids;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This convertor assumes that the provided string locators are formed by valid integer strings (within the integer range)
 *  and are only converted there and back again. The optional {@code length} parameter says that the string should be
 *  padded by left-size zeros to given length.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class DirectStringIntConvertor implements LocatorStringIntConvertor {

    /** Class serial id for serialization. */
    private static final long serialVersionUID = 8602101L;
    
    /** If the returned string should be left-padded by zeros, this string contains the prepared format string. */
    private final String formattingString;

    /**
     * Creates a new convertor with no left-sized padding.
     */
    public DirectStringIntConvertor() {
        formattingString = null;
    }    

    /**
     * Creates a new convertor with given fixed string length.
     * @param stringLocatorLength given this parameter says that the string should be padded by left-size zeros to given length.
     */
    public DirectStringIntConvertor(int stringLocatorLength) {
        if (stringLocatorLength > 0) {
            this.formattingString = "%0" + stringLocatorLength + "d";
        } else {
            this.formattingString = null;
        }
    }    
    
    // *******************     Implementation of the string -> int locator converter    ******************* //

    @Override
    public int getIntLocator(String locator) {
        try {
            return Integer.valueOf(locator);
        } catch (NumberFormatException ex) {
            Logger.getLogger(DirectStringIntConvertor.class.getName()).log(Level.WARNING, "passed string locator is not a valid integer: {0}", locator);
            return locator.hashCode();
        }
    }

    @Override
    public String getStringLocator(int id) {
        if (formattingString != null) {
            return String.format(formattingString, id);
        }
        return String.valueOf(id);
    }
}
