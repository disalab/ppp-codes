/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.algorithms;

import pppcodes.index.PPPCodeReadWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import messif.algorithms.Algorithm;
import messif.algorithms.NavigationProcessor;
import messif.algorithms.impl.MultipleOverlaysAlgorithm;
import messif.buckets.BucketStorageException;
import messif.operations.AbstractOperation;
import messif.operations.RankingSingleQueryOperation;
import messif.operations.data.BulkInsertOperation;
import messif.operations.data.DeleteOperation;
import messif.operations.data.InsertOperation;
import messif.operations.query.ApproxKNNQueryOperation;
import messif.operations.GetCandidateSetOperation;
import messif.operations.GetJoinCandidatesOperation;
import messif.operations.query.GetObjectCountOperation;
import messif.operations.query.GetRandomObjectsQueryOperation;
import mindex.algorithms.MIndexAlgorithm;
import pppcodes.PPPCodeIndex;
import pppcodes.PPPCodeIndexFile;
import pppcodes.ids.LocatorStringIntConvertor;
import pppcodes.index.PPPCodeInternalCell;
import pppcodes.processors.ApproxNavProcessorCandSet;
import pppcodes.processors.ApproxNavProcessorNorefine;
import pppcodes.processors.GetJoinCandidatesProcessor;
import pppcodes.processors.GetRandomNoDataObjectsNavigationProcessor;

/**
 * This is an algorithm for PPP Codes that internally uses several standard M-Index algorithms.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeAlgorithm extends MultipleOverlaysAlgorithm {
    
    /** Class id for serialization. */
    private static final long serialVersionUID = 202002L;

    /** Internal list of algorithms (the same as encapsulated ones from the constructor). */
    protected List<PPPCodeSingleAlgorithm> algorithms;

    /** Translator of string locators to unique integer IDs and back again. */
    protected LocatorStringIntConvertor locatorConvertor;
    
    /** A simple lock for periodic algorithm serialization. */
    protected transient Lock serializingLock = new ReentrantLock();
    
    /**
     * Creates a new multi-algorithm overlay for the given collection of algorithms.
     * @param algorithms the algorithms on which the operations are processed
     */
    @Algorithm.AlgorithmConstructor(description = "Constructor with created algorithms", arguments = {"array of running algorithms"})
    public PPPCodeAlgorithm(Algorithm[] algorithms) {
        this(algorithms, null);
    }

    /**
     * Creates a new multi-algorithm overlay for the given collection of algorithms.
     * @param algorithms the algorithms on which the operations are processed
     * @param locatorConvertor convertor used for string-integer locator conversion and back again
     */
    @Algorithm.AlgorithmConstructor(description = "Constructor with created algorithms", arguments = {"array of running algorithms", "converter of locator strings to integer"})
    public PPPCodeAlgorithm(Algorithm[] algorithms, LocatorStringIntConvertor locatorConvertor) {
        super("PPP-Codes algorithm with " + algorithms.length + " pivot spaces", Arrays.asList(algorithms), false);
        this.algorithms = Collections.unmodifiableList((List)Arrays.asList(algorithms));
        this.locatorConvertor = locatorConvertor;
    }
            
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.serializingLock  = new ReentrantLock();
    }    
    
    /**
     * Consolidate data in all leaf nodes of all dynamic Voronoi trees by putting together object IDs with the same PPP.
     */
    public void consolidateTreeData() {
        log.info("Consolidating memory data in all PPP-Trees");
        for (PPPCodeSingleAlgorithm mIndexAlgorithm : algorithms) {
            mIndexAlgorithm.getmIndex().consolidateTreeData();
        }
    }

    /**
     * This is just a testing method that should not be called under normal circumstances.
     * Reads all the data from PPP-Code file storage into the memory.
     * @throws BucketStorageException if the reading fails
     */
    public void reloadDataTest() throws BucketStorageException {
        for (PPPCodeSingleAlgorithm mIndexAlgorithm : algorithms) {
            ((PPPCodeIndexFile) mIndexAlgorithm.getmIndex()).reloadDataTest();
        }
    }

    public void findObject(int id) {
        int i = 0;
        for (PPPCodeSingleAlgorithm mIndexAlgorithm : algorithms) {
            System.out.println("alg " + i++);
            ((PPPCodeInternalCell) mIndexAlgorithm.getmIndex().getVoronoiCellTree()).findObject(id);
        }        
    }
   
    
    /**
     * Checks if there was any update operation executed on this algorithm since the last serialization
     *  and call the serialization, if it there was any update.
     * @param serializationFile file to serialize the whole algorithm into
     * @throws IOException if the serialization failed
     */
    public void checkModifiedAndStore(String serializationFile) throws IOException {
        serializingLock.lock();
        try {
            if (! isUpdateLogEmpty()) {
                this.storeToFile(serializationFile);
            }
        } finally {
            serializingLock.unlock();
        }
    }

    @Override
    protected void beforeStoreToFile(String filepath) {
        super.beforeStoreToFile(filepath);
        for (MIndexAlgorithm mIndexAlgorithm : algorithms) {
            mIndexAlgorithm.beforeStoreToFile(filepath);
        }
    }

    @Override
    protected void afterStoreToFile(String filepath, boolean successful) {
        super.afterStoreToFile(filepath, successful); 
        for (MIndexAlgorithm mIndexAlgorithm : algorithms) {
            mIndexAlgorithm.afterStoreToFile(filepath, successful);
        }
    }        
    
    /**
     * Sets a new locator convertor for this PPP-Code algorithm. Use this method only if you very
     *  well know, what you're doing so that the serialized data is still compatible with the new 
     *  convertor.
     * @param locatorConvertor new convertor from string locators to the integer ones.
     */
    public void setLocatorConvertor(LocatorStringIntConvertor locatorConvertor) {
        this.locatorConvertor = locatorConvertor;
    }
    
    // **********************************       Overriding methods      ******************************* //
    
    @Override
    public void setOperationsThreadPool(ExecutorService operationsThreadPool) {
        super.setOperationsThreadPool(operationsThreadPool); //To change body of generated methods, choose Tools | Templates.
        for (Algorithm algorithm : algorithms) {
            if (algorithm.getOperationsThreadPool() == null || algorithm.getOperationsThreadPool().isShutdown()) {
                algorithm.setOperationsThreadPool(operationsThreadPool);
                //System.out.println("setting thread pool also to sub-algorithm");
            }
        }
    }
    
    /** Pre-created list of supported operations. */
    private final static List<Class<? extends AbstractOperation>> supportedOperations = 
            Collections.unmodifiableList(Arrays.asList(BulkInsertOperation.class, InsertOperation.class, DeleteOperation.class, 
                ApproxKNNQueryOperation.class, GetCandidateSetOperation.class, GetRandomObjectsQueryOperation.class, GetObjectCountOperation.class, GetJoinCandidatesOperation.class));

    @Override
    public List<Class<? extends AbstractOperation>> getSupportedOperations() {
        return PPPCodeAlgorithm.supportedOperations;
    }        
    
    @Override
    public <E extends AbstractOperation> List<Class<? extends E>> getSupportedOperations(Class<? extends E> subclassToSearch) {
        return Algorithm.getOperationSubClasses(getSupportedOperations(), subclassToSearch);
    }
    
    @Override
    public NavigationProcessor<? extends AbstractOperation> getNavigationProcessor(AbstractOperation operation) {
        if ((operation instanceof InsertOperation) || (operation instanceof BulkInsertOperation) || (operation instanceof DeleteOperation)) {
            return super.getNavigationProcessor(operation);
        }
        if (operation instanceof ApproxKNNQueryOperation || operation instanceof GetCandidateSetOperation) {
            List<PPPCodeSingleAlgorithm> indexesToRunOn;
            // EXPERIMENTS ONLY: if the number of used overlays is limited: TEST
            if (operation.containsParameter("MAX_LAMBDA")) {
                indexesToRunOn = new ArrayList<>(algorithms.subList(0, operation.getParameter("MAX_LAMBDA", Integer.class, algorithms.size())));
            } else {
                indexesToRunOn = new ArrayList<>(algorithms);
            }

            if (operation instanceof ApproxKNNQueryOperation) {
                // Approximate operations is evaluated without any refinement
                return new ApproxNavProcessorNorefine((RankingSingleQueryOperation) operation, indexesToRunOn, locatorConvertor, ((ApproxKNNQueryOperation) operation).getK());
            } else {
                return new ApproxNavProcessorCandSet((GetCandidateSetOperation) operation, indexesToRunOn, locatorConvertor);
            }
        }
        if (operation instanceof GetRandomObjectsQueryOperation) {
            if (algorithms.isEmpty()) {
                return null;
            }
            return new GetRandomNoDataObjectsNavigationProcessor((GetRandomObjectsQueryOperation) operation, 
                    algorithms.get(0).getmIndex().getVoronoiCellTree(), locatorConvertor);
        }
        
        if (operation instanceof GetJoinCandidatesOperation) {
            return new GetJoinCandidatesProcessor((GetJoinCandidatesOperation) operation, algorithms.get(0).getmIndex(), locatorConvertor);
        }
        return null;
    }
    
    public void processGetObjectCount(GetObjectCountOperation op) {
        if (! algorithms.isEmpty()) {
            int i = 1;
            for (PPPCodeSingleAlgorithm algorithm : algorithms) {
                System.out.println("algorithm " + (i++) + " count: " + algorithm.getmIndex().getObjectCount() +
                        ", by object iterator: " + algorithm.getmIndex().getVoronoiCellTree().getObjectCountByObjects());
            }
            op.addToAnswer(algorithms.get(0).getmIndex().getObjectCount());            
        }
        op.endOperation();
    }

    @Override
    public String toString() {
        StringBuilder strBuf = new StringBuilder("PPPCode algorithm with the following parameters: \n");
        strBuf.append("lambda = ").append(algorithms.size()).append(" (number of overlays)\n");
        if (algorithms.size() <= 0) {
            return strBuf.toString();
        }
        strBuf.append("k = ").append(algorithms.get(0).getmIndex().getNumberOfPivots()).append("\n");
        strBuf.append("l = ").append(algorithms.get(0).getmIndex().getMaxLevel()).append("\n");
        strBuf.append("cap = ").append(algorithms.get(0).getmIndex().getBucketCapacity()).append("\n");
        strBuf.append("class = ").append(algorithms.get(0).getmIndex().getObjectClass()).append("\n");
        
        return strBuf.toString();
    }
    
    /**
     * Prints the rich information about this PPP-Code algorithm including overall numbers of internal and
     *  leaf cells of the Voronoi trees and the bytes occupied by all byte arrays in the leaf cells.
     * @param serializationFile file name where the whole PPP-Code multi-algorithm was serialized (for calculating sizes)
     * @return formatted string with the information
     */
    public String toRichString(String serializationFile) {
        StringBuilder strBuf = new StringBuilder(toString());

        if (algorithms.size() <= 0) {
            return strBuf.toString();
        
        }
        PPPCodeIndex oneMIndex = algorithms.get(0).getmIndex();
        PPPCodeReadWriter pppCodeReadWriter = oneMIndex.getPPPCodeReadWriter();
        // print automatically calculated capacities 
//        for (short i = 0; i < oneMIndex.getMaxLevel(); i++) {
//            strBuf.append("optimal capacity on level ").append(i).append(" = ").append(pppCodeReadWriter.getOptimalLeafCapacityBytes(i))
//                    .append(" bytes = ").append(pppCodeReadWriter.getOptimalLeafCapacityBytes(i)/pppCodeReadWriter.getPPPCodeObjectBinarySize(i)).append(" objs")
//                    .append("; max branching: ").append(pppCodeReadWriter.getMaxBranching(i)).append("\n");
//        }
        
        // print the size of the Voronoi cell tree and of the data stored in all the indexes
        int objCount = oneMIndex.getObjectCount();
        Map<Short,AtomicInteger> intCellNumbers = new HashMap<>(oneMIndex.getMaxLevel());
        Map<Short, AtomicLong> branchingSums = new HashMap<>(oneMIndex.getMaxLevel());
        for (short i = 0; i < oneMIndex.getMaxLevel(); i++) {
            intCellNumbers.put(i, new AtomicInteger());
            branchingSums.put(i, new AtomicLong());
        }        
        
        AtomicInteger leafCellNumber = new AtomicInteger();
        AtomicLong dataSizeBytes = new AtomicLong();
        AtomicLong leafLevelSum = new AtomicLong();
        for (PPPCodeSingleAlgorithm mIndexAlgorithm : algorithms) {
            mIndexAlgorithm.getmIndex().getTreeSize(intCellNumbers, branchingSums, leafCellNumber, dataSizeBytes, leafLevelSum);
        }

        strBuf.append("\nnr. of objs = ").append(objCount).append("\n");        
        int intCellNumber = 0;
        for (short i = 0; i < oneMIndex.getMaxLevel(); i++) {
            intCellNumber += intCellNumbers.get(i).get();
            strBuf.append("\t int. cells on lvl ").append(i).append(" = ").append(intCellNumbers.get(i).get())
                    .append("; avg. branching = ").append((float) branchingSums.get(i).get() / (float) intCellNumbers.get(i).get()).append("\n");
        }
        strBuf.append("overall nr. of int. cells = ").append(intCellNumber).append("\n");
        
        strBuf.append("nr. of leaf cells = ").append(leafCellNumber.get()).append("\n");
        strBuf.append("avg leaf cell lvl = ").append((float) leafLevelSum.get() / (float) leafCellNumber.get()).append("\n");
        strBuf.append("data size (bytes) = ").append(dataSizeBytes.get()).append("\n\n");

        float bytesSize;
        if (new File(serializationFile).exists()) {
            bytesSize = (float) new File(serializationFile).length() / (float) objCount;
            strBuf.append("avg full obj size = ").append(bytesSize).append(" bytes = ").append((int) (bytesSize * 8f)).append(" bits\n");
        }
                
        bytesSize = (float) dataSizeBytes.get() / (float) objCount;
        strBuf.append("avg data obj size = ").append(bytesSize).append(" bytes = ").append((int) (bytesSize * 8f)).append(" bits\n");
        bytesSize = bytesSize - ((algorithms.size() * pppCodeReadWriter.getIDBitSize()) / 8f);
        strBuf.append("avg ppp-code size = ").append(bytesSize).append(" bytes = ").append((int) (bytesSize * 8f)).append(" bits\n\n");

        bytesSize = (pppCodeReadWriter.getPPPCodeBitSize(oneMIndex.getMaxLevel()) * algorithms.size() + pppCodeReadWriter.getIDBitSize()) / 8f;
        strBuf.append("seq. scan data obj size = ").append(bytesSize).append(" bytes = ").append((int) (bytesSize * 8f)).append(" bits\n");
        bytesSize = (pppCodeReadWriter.getPPPCodeObjectBitSize(0) * algorithms.size()) / 8f;
        strBuf.append("data obj size with no tree = ").append(bytesSize).append(" bytes = ").append((int) (bytesSize * 8f)).append(" bits\n");
        bytesSize = bytesSize  - ((algorithms.size() * pppCodeReadWriter.getIDBitSize()) / 8f);
        strBuf.append("theoretical ppp-code size = ").append(bytesSize).append(" bytes = ").append((int) (bytesSize * 8f)).append(" bits\n\n");
        
        return strBuf.toString();
        
    }

}
