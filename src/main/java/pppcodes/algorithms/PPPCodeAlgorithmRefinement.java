/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.algorithms;

import pppcodes.processors.ApproxNavProcessorRefinement;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import messif.algorithms.Algorithm;
import messif.algorithms.NavigationProcessor;
import messif.buckets.BucketStorageException;
import messif.objects.LocalAbstractObject;
import messif.operations.AbstractOperation;
import messif.operations.RankingSingleQueryOperation;
import messif.operations.data.BulkInsertOperation;
import messif.operations.data.InsertOperation;
import messif.operations.query.ApproxKNNQueryOperation;
import messif.operations.GetCandidateSetOperation;
import messif.utility.ExtendedProperties;
import mindex.MetricIndexes;
import pppcodes.ids.IDObjectRAStorage;
import pppcodes.ids.PPPDiskIndex;
import pppcodes.ids.ObjectLocatorConvertor;
import pppcodes.processors.ApproxNavProcessorCandSet;
import pppcodes.processors.ApproxNavProcessorNorefine;

/**
 * This is an algorithm for PPP Codes has a built-in ID-object storage of objects for refinement.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class PPPCodeAlgorithmRefinement extends PPPCodeAlgorithm {
    
    /** Class id for serialization. */
    private static final long serialVersionUID = 203001L;

    /** Random access storage of objects indexed according to integer IDs */
    protected transient IDObjectRAStorage idObjectStorage;
        
    /** Translator of string locators to unique integer IDs that stores the  */
    protected transient ObjectLocatorConvertor objectLocatorConvertor;
    
    /**
     * Creates a new multi-algorithm overlay for the given collection of algorithms.
     * @param algorithms the algorithms on which the operations are processed
     */
    @Algorithm.AlgorithmConstructor(description = "Constructor with created algorithms", arguments = {"array of running algorithms"})
    public PPPCodeAlgorithmRefinement(Algorithm[] algorithms) {
        super(algorithms);
    }

    /**
     * Creates a new multi-algorithm overlay for the given collection of algorithms.
     * @param algorithms the algorithms on which the operations are processed
     * @param configuration configuration valid for the whole PPP-Code algorithm (separate from individual PPP-Code trees)
     */
    @Algorithm.AlgorithmConstructor(description = "Constructor with created algorithms", arguments = {"array of running algorithms", "properties with PPP-Code configuration"})
    public PPPCodeAlgorithmRefinement(Algorithm[] algorithms, ExtendedProperties configuration) {
        this(algorithms);
        createPPPDiskIndex(configuration);
    }
    
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        if (idObjectStorage != null && idObjectStorage instanceof PPPDiskIndex) {
            ((PPPDiskIndex) idObjectStorage).storeToFile();
        }
    }
    
    
    // ******************     ID-object storage and int ID translation       ***************** //

    /**
     * Creates a {@link PPPDiskIndex} from configuration in properties.
     * @param configuration properties with settings
     */
    public final void createPPPDiskIndex(ExtendedProperties configuration) {
        if (configuration.getBoolProperty("createDiskIndex", false)) {
            try {
                PPPDiskIndex index = PPPDiskIndex.createInDir(configuration);
                idObjectStorage = index;
                objectLocatorConvertor = index;
            } catch (IOException ex) {
                MetricIndexes.logger.severe(ex.toString());
            }
        }        
    }

    /**
     * The list of inserted objects is stored in the {@link PPPCodeAlgorithm#idObjectStorage} and
     *  each the string locators are replaced by integer IDs.
     * @param insertedObjects list of objects to be inserted into this algorithm
     * @return false, if storing of objects fails; true, otherwise
     */
    protected boolean preprocessStoredObjects(Collection<? extends LocalAbstractObject> insertedObjects) {
        if (objectLocatorConvertor != null) {
            for (LocalAbstractObject obj : insertedObjects) {
                objectLocatorConvertor.getAndStoreIntLocator(obj);
            }
        }
        if (idObjectStorage != null) {
            for (LocalAbstractObject obj : insertedObjects) {
                try {
                    idObjectStorage.storeObject(idObjectStorage.preprocessObject(obj));
                } catch (BucketStorageException ex) {
                    MetricIndexes.logger.severe("id-object storage failed: " + ex.getLocalizedMessage());
                    return false;
                }
            }
        }
        return true;
    }
    
    // **********************************       Overriding methods      ******************************* //
        
    @Override
    public NavigationProcessor<? extends AbstractOperation> getNavigationProcessor(AbstractOperation operation) {
        if ((operation instanceof InsertOperation) || (operation instanceof BulkInsertOperation)) {
            if (! preprocessStoredObjects((operation instanceof InsertOperation) ? 
                    Collections.singletonList(((InsertOperation) operation).getInsertedObject()) : ((BulkInsertOperation) operation).getInsertedObjects())) 
                return null;
            return super.getNavigationProcessor(operation);
        }
        
        if (operation instanceof ApproxKNNQueryOperation || operation instanceof GetCandidateSetOperation) {
            List<PPPCodeSingleAlgorithm> indexesToRunOn;
            // EXPERIMENTS ONLY: if the number of used overlays is limited: TEST
            if (operation.containsParameter("MAX_LAMBDA")) {
                indexesToRunOn = new ArrayList<>(algorithms.subList(0, operation.getParameter("MAX_LAMBDA", Integer.class, algorithms.size())));
            } else {
                indexesToRunOn = new ArrayList<>(algorithms);
            }

            if (operation instanceof ApproxKNNQueryOperation) {
                if (idObjectStorage == null || operation.getParameter("NOREFINE", Boolean.class, false)) {                
                    // Approximate operations is evaluated without any refinement
                    return new ApproxNavProcessorNorefine((RankingSingleQueryOperation) operation, indexesToRunOn, (locatorConvertor == null) ? objectLocatorConvertor : locatorConvertor, ((ApproxKNNQueryOperation) operation).getK());
                } else {
                    return new ApproxNavProcessorRefinement(idObjectStorage, (ApproxKNNQueryOperation) operation, indexesToRunOn);
                }
            } else {
                return new ApproxNavProcessorCandSet((GetCandidateSetOperation) operation, indexesToRunOn, (locatorConvertor == null) ? objectLocatorConvertor : locatorConvertor);
            }
        }        
        return null;
    }

}
