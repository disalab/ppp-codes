/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.processors;

import gnu.trove.iterator.TIntIterator;
import java.util.NoSuchElementException;
import java.util.concurrent.ArrayBlockingQueue;
import messif.algorithms.AlgorithmMethodException;
import messif.operations.RankingSingleQueryOperation;
import mindex.MetricIndex;
import mindex.processors.ApproxNavigationProcessor;
import mindex.processors.SinglePPPRankable;
import pppcodes.index.LocatorsAndDistance;
import pppcodes.index.RankableLocators;

/**
 * Asynchronous processor that takes care of one PPP-Tree. It manages a queue of prepared candidate IDs from given
 *  tree (pivot space).
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class SingleSpaceCandGeneratorAsync extends SingleSpaceCandGenerator {

    /** Number of objects to be prepared in the cache for immediate reading. If not, the sub-processing is started */
    public static final int CACHE_SIZE = 256;
    
    /** If true, the processing of this processor should be ended */
    private volatile boolean readingFinished = false;
    
    /** Cache of objects prepared for reading from this processor. */
    protected final ArrayBlockingQueue<RankableLocators> objectCache;
    
    /** The first object of the cache */
    protected RankableLocators firstObject = null;
    
    /**
     * Creates new approximate navigation processor for given operation and M-Index. The priority queue is initialized.
     * @param operation approximate operation - typically kNN or range (should be the same for approximate range)
     * @param mIndex M-Index object with configuration and dynamic cell tree
     * @throws AlgorithmMethodException if the M-Index cannot determine the PP for query object
     */
    public SingleSpaceCandGeneratorAsync(RankingSingleQueryOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        super(operation, mIndex);
        objectCache = new ArrayBlockingQueue<>(CACHE_SIZE);
    }  

    /**
     * This method reads from the priority queue {@link #priorityQueue} next leaf node of the PPP-Tree. 
     *   The underlying {@link ApproxNavigationProcessor} processes any internal nodes that would be on the top of the
     *   priority queue. If data objects (object IDs, instances of {@link RankableLocators}) are at the top of the queue, 
     *   they are moved to the cache {@link #objectCache}.
     * @param isAsynchronous flag, if the processing of this processor is realized in threads (it should not be in case of this class)
     * @return leaf cell or null, if the reading is finished (the PPP-Codes merging thread has finished)
     */
    @Override
    protected RankableCell getNextProcessingItem(boolean isAsynchronous) {
        RankableCell retVal = null;
        while (((retVal = super.getNextProcessingItem(isAsynchronous)) == null) && !readingFinished && !priorityQueue.isEmpty()) {
            moveFromQueueToCache();
        }
        if (readingFinished) {
            if (retVal != null) {
                retVal.getCell().readUnLock();
            }
            return null;
        }
        return retVal;
    }
    
    @Override
    protected RankingSingleQueryOperation processItem(RankingSingleQueryOperation operation, SinglePPPRankable processingItem) throws AlgorithmMethodException {
        try {
            RankingSingleQueryOperation retVal = super.processItem(operation, processingItem);
            if (priorityQueue.peek() instanceof RankableLocators) {
                moveFromQueueToCache();
            }
            return retVal;
        } catch (Throwable ex) {
            System.out.println("catching throwable in SingleSpaceCandGeneratorAsync#processItem");
//            objectCache.
            ex.printStackTrace();
            try {
                objectCache.put(new LocatorsAndDistance(0f, new int [] { - 1 }));
            } catch (InterruptedException ignore) {}
            return operation;
        }
    }    
    
    /**
     * 
     * @throws NoSuchElementException 
     */
    private void moveFromQueueToCache() throws NoSuchElementException {
        while (! priorityQueue.isEmpty() && (priorityQueue.peek() instanceof RankableLocators) && !readingFinished) {            
            try {
                objectCache.put(((RankableLocators) priorityQueue.poll()).readLocators());
                queueItemsProcessed ++;
            } catch (InterruptedException ex) {
                throw new NoSuchElementException(ex.getLocalizedMessage());
            }
        }
    }
    
    /**
     * Creates and returns an iterator over the PPP codes from the head of the queue
     *  that have the same distance (typically objects with the same PPP code).
     * @return iterator over PPP-code objects that should be processed
     */
    @Override
    public TIntIterator getTopIDs() {
        return new FirstSetIteratorAsync();
    }    
    
    /**
     * Iterator over the maintained queue of prepared IDs from given PPP-Code space.
     */
    protected class FirstSetIteratorAsync implements TIntIterator {
        // distance to the last returned object
        float lastDistance = Float.MAX_VALUE;
        // flag if the next object can be returned
        boolean hasNext = false;
        int [] topInts = null;
        int topIntIndex = 0;
                
        @Override
        public boolean hasNext() {
            if (hasNext || (topInts != null && topIntIndex < topInts.length)) {
                return hasNext = true;
            }
            try {
                if (firstObject == null) {
                    // this is a blocking operation
                    firstObject = objectCache.take();
                }
                return hasNext = (firstObject.getQueryDistance() <= lastDistance);
            } catch (InterruptedException ignore) {
                throw new NoSuchElementException(ignore.getLocalizedMessage());
            }
        }

        @Override
        public int next() {
            if (! hasNext) {
                throw new NoSuchElementException("all objects with the same distance were returned (or hasNext() not called)");
            }
            try {
                if (topInts == null || topIntIndex >= topInts.length) {
                    queueItemsProcessed ++;
                    topInts = firstObject.getLocators();
                    topIntIndex = 0;
                    lastDistance = firstObject.getQueryDistance();
                    firstObject = null;
                }
                return topInts[topIntIndex];
            } finally {
                hasNext = false;
                topIntIndex ++;
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("This is a read only iterator");
        }
    }    
    
    /**
     * Finish the thread that takes care about reading and sorting the PPP-Code data from leaf cells.
     */
    @Override
    public void finishReading() {
        super.finishReading();
        readingFinished = true;
        objectCache.poll();
    }
    
}

