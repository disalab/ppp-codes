/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.processors;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.procedure.TIntIntProcedure;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.AsynchronousNavigationProcessor;
import messif.algorithms.NavigationProcessors;
import messif.operations.AbstractOperation;
import messif.operations.RankingSingleQueryOperation;
import messif.statistics.OperationStatistics;
import messif.statistics.StatisticCounter;
import messif.statistics.StatisticTimer;
import messif.statistics.Statistics;
import mindex.MetricIndexes;
import mindex.algorithms.MIndexAlgorithm;
import pppcodes.algorithms.PPPCodeSingleAlgorithm;

/**
 * Navigation processor for PPPCode M-Index (with multiple M-Index overlays).
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public abstract class ApproxNavProcessorPPPCodes implements AsynchronousNavigationProcessor<AbstractOperation> {

    /**
     * The algorithm stores necessary statistics for each of these k-NN (length
     * of the sublist necessary to achieve the percentile for required "k"
     * number of objects, etc.
     */
    public final static int[] KS_OF_INTEREST = new int[] {1, 10, 100, 500, 1000, 1500, 2000, 3000, 5000, 10000};

    /** Name of parameter to manage the percentile. */
    public final static String PARAM_PERCENTILE = "PERCENTILE";
    
    /** Default value of the percentile */
    public final static float DEFAULT_PERCENTILE = 0.5f;
    
    /** The original operation. */
    protected final RankingSingleQueryOperation originalOperation;
    
    /** The list of M-Index overlays to start the processing on. */
    private final List<PPPCodeSingleAlgorithm> algoritmsToRun;
    
    /** The list of active processors to merge. */
    private final List<SingleSpaceCandGenerator> candidateSubLists;    
    
    /** The minimal number of the rank distribution to report out (default is median = Math.ceil(0.5 * nIndexes) ). */
    protected final int minHits;
        
    /**
     * Creates new approximate navigation processor for given operation list of individual PPP-Code indexes. 
     *  A default percentile 0.5 (median) is used for merging individual sub-indexes priority queues.
     * @param operation approximate kNN operation (should be the same for approximate range)
     * @param indexes list of sub-indexes
     */
    protected ApproxNavProcessorPPPCodes(RankingSingleQueryOperation operation, List<PPPCodeSingleAlgorithm> indexes) {
        this.originalOperation = operation;
        this.algoritmsToRun = Collections.synchronizedList(indexes);
        this.candidateSubLists = Collections.synchronizedList(new ArrayList<SingleSpaceCandGenerator>(indexes.size()));
        
        float percentile = operation.getParameter(PARAM_PERCENTILE, Float.class, DEFAULT_PERCENTILE);
        this.minHits = (int) Math.ceil(percentile * indexes.size());
        
        // statistics
        this.operationTime = OperationStatistics.getOpStatistics("OperationTime", StatisticTimer.class);
        operationTime.reset();
        operationTime.start();        
    }    

    @Override
    public AbstractOperation getOperation() {
        return originalOperation;
    }
    
    @Override
    public Callable<AbstractOperation> processStepAsynchronously() throws InterruptedException {
        try {
            final MIndexAlgorithm removed = algoritmsToRun.remove(0);
            final SingleSpaceCandGeneratorAsync processor = new SingleSpaceCandGeneratorAsync(originalOperation, removed.getmIndex());
            //final SingleApproxNavProcessorAsyncTop processor = new SingleApproxNavProcessorAsyncTop(originalOperation, removed.getmIndex());
            candidateSubLists.add(processor);
            return new Callable<AbstractOperation>() {
                @Override
                public RankingSingleQueryOperation call() throws InterruptedException, CloneNotSupportedException, AlgorithmMethodException {
                    // the single navigation processor CANNOT be executed in threads (that is why pass 'null' to the .execute method)
                    NavigationProcessors.execute(null, processor);
                    return originalOperation;
                }
            };
        } catch (IndexOutOfBoundsException e) {
            // run the PercRank algorithm over list of rankings, if not run yet
            if (percRankStarted.getAndSet(true)) {
                return null;
            }
            return new Callable<AbstractOperation>() {
                @Override
                public RankingSingleQueryOperation call() throws InterruptedException, CloneNotSupportedException, AlgorithmMethodException {
                    mergeByMedianRankAggregation();
                    return originalOperation;
                }
            };            
        } catch (AlgorithmMethodException e) {
            MetricIndexes.logger.warning(e.getMessage());
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
    }

    /**
     * When processing sequentially (without the thread pool set), then it is in one step only.
     * @return always false (no other step is done)
     * @throws java.lang.InterruptedException if the thread was interrupted from the outside
     * @throws java.lang.CloneNotSupportedException if the operation cannot be clonned
     */
    @Override
    public boolean processStep() throws InterruptedException, AlgorithmMethodException, CloneNotSupportedException {
        for (MIndexAlgorithm mIndexAlgorithm : algoritmsToRun) {
            candidateSubLists.add(new SingleSpaceCandGenerator(originalOperation, mIndexAlgorithm.getmIndex()));
        }
        mergeByMedianRankAggregation();
        return false;
    }

    @Override
    public boolean isFinished() {
        return percRankEnded.get();
    }

    @Override
    public void close() {
        //statistics handling
        if (! candidateSubLists.isEmpty()) {
            addCurrentStatistics("");
        }
        for (StatisticCounter stat : statsToOutput) {
            OperationStatistics.getOpStatisticCounter(stat.getName()).set(stat.get());
        }
    }
    
    @Override
    public int getProcessedCount() {
        return candidateSubLists.size() + (percRankStarted.get() ? 1 : 0);
    }

    @Override
    public int getRemainingCount() {
        if (algoritmsToRun.isEmpty()) {
            return percRankStarted.get() ? 0 : 1;
        }
        return algoritmsToRun.size() + 1;
    }
    
        
    // *******************     PercRank merge   ******************* //
    
    protected  AtomicBoolean percRankStarted = new AtomicBoolean(false);
    protected  AtomicBoolean percRankEnded = new AtomicBoolean(false);
    
    /** Elements that were processed in when we traverse the pre-ranked lists managed by object locators. */
    protected TIntIntMap seenElements = new TIntIntHashMap(10000);
    
    /** Object with frequencies that exceeded the minimal required frequency. */
    protected TIntIntMap toReport = new TIntIntHashMap(500);
    
    /** Elements that were already stored to the global answer should not be considered any more */
    protected TIntSet reportedElements = new TIntHashSet(1500);

    
    /** 
     * This method merges individual answer lists pre-ranked from individual overlays
     *  into a single global answer. It uses a modification of the Median Rank algorithm
     *  by Fagin et al.
     */
    protected void mergeByMedianRankAggregation() {
        try{
            boolean changed = true;
            MainLoop:
            do {
                if (! changed) {
                    MetricIndexes.logger.log(Level.WARNING, "not enough sublist length for: {0}, lambda={1}, perc={2} hits: so far having {3} objects", new Object[]{originalOperation.getQueryObject().getLocatorURI(), candidateSubLists.size(), minHits, originalOperation.getAnswerCount()});
                    return;
                }
                changed = false;
                nIterations ++;
                
                // for each list from individual M-Index overlays
                for (SingleSpaceCandGenerator candidateSubList : candidateSubLists) {
                    TIntIterator it = candidateSubList.getTopIDs();
                    int itCounter = 0;
                    // access all objects from a single Voronoi cell
                    while (it.hasNext()) {
                        changed = true;
                        int currentObjectID = it.next();
                        if (currentObjectID < 0) {
                            break MainLoop;
                        }
                        itCounter ++;
                        int objectFrequency = seenElements.adjustOrPutValue(currentObjectID, 1, 1);
                        if (objectFrequency >= minHits) {
                            // add object to "toReport", if it is not there already
                            if (! reportedElements.contains(currentObjectID)) {
                                toReport.adjustOrPutValue(currentObjectID, 1, objectFrequency);
                            }
                        }
                    }
                    probeDepthSum += itCounter;
                    if (singleReadMax < itCounter) {
                        singleReadMax = itCounter;
                    }
                }

                // iterate via the objects that reached the frequency limit
                if (! toReport.isEmpty()) {
                    final int [] [] entries = new int [toReport.size()] [];
                    toReport.forEachEntry(new TIntIntProcedure() {
                        int i = 0;
                        @Override
                        public boolean execute(int a, int b) {
                            entries[i ++] = new int [] {a, b};
                            return true;
                        }
                    });
                    Arrays.sort(entries, freqeuencyComparator);
                    
                    for (int[] entry : entries) {
                        addIDToCandidateSet(entry[0]);
                        reportedElements.add(entry[0]);
                    }
                    toReport.clear();
                }            
            } while (continueMerging());

            percRankEnded.set(true);
        } catch (ConcurrentModificationException e) {
            MetricIndexes.logger.severe(e.getLocalizedMessage());
            e.printStackTrace();
        } finally {
            for (SingleSpaceCandGenerator candidateQueue : candidateSubLists) {
                candidateQueue.finishReading();
            }
        }
    }

    /**
     * Method tat stores given integer ID to the candidate set. Method to be override in extending classes.
     * @param id object ID to be added to candidate set
     */
    protected abstract void addIDToCandidateSet(int id);
    
    /**
     * Returns true if the next merging step should be done, false otherwise. This method is expected to be overridden.
     * @return true/false if the merging should continue
     */
    protected abstract boolean continueMerging();
    
    
    // ********************************         Statistics       ******************************************** //
    
    private int reachedKOfInterest = -1;
    private int probeDepthSum = 0;
    private int singleReadMax = 0;
    private int nIterations = 0;
    private final List<StatisticCounter> statsToOutput = new ArrayList<>();
    private final StatisticTimer operationTime;    

    protected void updateStatistics(int candidatesProcessed) throws ClassCastException {
        //            while (reachedKOfInterest < KS_OF_INTEREST.length - 1 && originalOperation.getAnswerCount() >= KS_OF_INTEREST[reachedKOfInterest + 1]) {
        while (reachedKOfInterest < KS_OF_INTEREST.length - 1 && candidatesProcessed >= KS_OF_INTEREST[reachedKOfInterest + 1]) {
            reachedKOfInterest++;
            addCurrentStatistics("-" + KS_OF_INTEREST[reachedKOfInterest]);
        }
    }
    

    protected void addCurrentStatistics(String suffix) throws ClassCastException {
        if (! Statistics.isEnabledGlobally()) {
            return;
        }
        // set statistics of the maximal sublist length
        StatisticCounter maxSublistLengthK = OperationStatistics.getOpStatisticCounter("RankingLength" + suffix);
        statsToOutput.add(maxSublistLengthK);
        maxSublistLengthK.set(probeDepthSum / candidateSubLists.size());

        // the sum of PPP-Code objects read from all sub-lists
        long nodesInQueueSum = 0;
        long objectsInQueueSum = 0;
        long queueItemsProcessed = 0;
        for (SingleSpaceCandGenerator processor : candidateSubLists) {
            nodesInQueueSum += processor.nodesAddedToQ;
            objectsInQueueSum += processor.objectsAddedToQ;
            queueItemsProcessed += processor.queueItemsProcessed;
        }
        StatisticCounter itemsAddedStatK = OperationStatistics.getOpStatisticCounter("NodesInQueue" + suffix);
        statsToOutput.add(itemsAddedStatK);
        itemsAddedStatK.set(nodesInQueueSum / candidateSubLists.size());
        
        itemsAddedStatK = OperationStatistics.getOpStatisticCounter("DataObjectsInQueue" + suffix);
        statsToOutput.add(itemsAddedStatK);
        itemsAddedStatK.set(objectsInQueueSum / candidateSubLists.size());
        
        itemsAddedStatK = OperationStatistics.getOpStatisticCounter("ObjectsInQueue" + suffix);
        statsToOutput.add(itemsAddedStatK);
        itemsAddedStatK.set((nodesInQueueSum + objectsInQueueSum) / candidateSubLists.size());

        itemsAddedStatK = OperationStatistics.getOpStatisticCounter("QueueSteps" + suffix);
        statsToOutput.add(itemsAddedStatK);
        itemsAddedStatK.set(queueItemsProcessed / candidateSubLists.size());

        itemsAddedStatK = OperationStatistics.getOpStatisticCounter("RankIterations" + suffix);
        statsToOutput.add(itemsAddedStatK);
        itemsAddedStatK.set(nIterations);

        itemsAddedStatK = OperationStatistics.getOpStatisticCounter("MaxCellSize" + suffix);
        statsToOutput.add(itemsAddedStatK);
        itemsAddedStatK.set(singleReadMax);
        
        // operation time of partial answer
        if ((suffix != null) && (! suffix.isEmpty())) {
            StatisticCounter operationTimeK = OperationStatistics.getOpStatisticCounter("OperationTime" + suffix);
            statsToOutput.add(operationTimeK);
            operationTimeK.set(operationTime.get());
        }
    }
    
    private final static Comparator<int[]> freqeuencyComparator = new Comparator<int[]>() {
        @Override
        public int compare(int[] o1, int[] o2) {
            return Integer.compare(o2[1], o1[1]);
        }
    };
}
