/*
 *  This file is part of PPP-Codes library: http://disa.fi.muni.cz/results/software/ppp-codes/
 *
 *  PPP-Codes library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  PPP-Codes library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with PPP-Codes library.  If not, see <http://www.gnu.org/licenses/>.
 */
package pppcodes.processors;

import gnu.trove.list.TFloatList;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TFloatArrayList;
import gnu.trove.list.array.TIntArrayList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import mindex.processors.SinglePPPRankable;

/**
 * This is implementation of a priority queue where only the top items are kept sorted. The rest of the items are
 *  kept in unsorted chunks, each chunk corresponding to an interval. The top is organized by standard java implementation
 *  of {@link PriorityQueue}. Not all methods are implemented (e.g. iterator() ).
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 * @param <T> type of items to be organized by this queue
 */
public class PartialPriorityQueue<T extends SinglePPPRankable> extends PriorityQueue<T> {

    /** Size of the first data chunk (512 objects). */
    protected static final int INITIAL_CHUNK_MAX_SIZE = 512;
    
    /** The size of chunks grows by this factor (1.5). */
    protected static final float INCREASE_FACTOR = 1.5f;

    /**
     * If the total size of the collection exceeds this limit, the last partition is removed (unless the size would fall under the {@link #MINIMUM_SIZE_LIMIT})
     */
    protected static final int REDUCE_SIZE_LIMIT = 200000;
    protected static final int MINIMUM_SIZE_LIMIT = 100000;   
    
    /** List of the unsorted chunks. */
    List<ArrayList<T>> unsortedChunks = new ArrayList<>();
    
    /** Borders of the intervals covered by individual chunks. */
    TFloatList chunkBorders = new TFloatArrayList();    
    
    /** Capacities of individual chunks. */
    TIntList chunkCapacities = new TIntArrayList();
   
    /**
     * Create new priority queue with given initial capacity and item comparator.
     * @param initialCapacity initial queue size
     * @param comparator item comparator
     * @throws IllegalArgumentException if the {@link PriorityQueue} constructor throws this exception
     */
    public PartialPriorityQueue(int initialCapacity, Comparator<? super T> comparator) throws IllegalArgumentException {
        super(initialCapacity, comparator);
        chunkBorders.add(Float.MAX_VALUE);
        chunkCapacities.add(INITIAL_CHUNK_MAX_SIZE);
    }

    /**
     * Create new priority queue with given item comparator.
     * @param comparator item comparator
     * @throws IllegalArgumentException if the {@link PriorityQueue} constructor throws this exception
     */
    public PartialPriorityQueue(Comparator<? super T> comparator) throws IllegalArgumentException {
        this(INITIAL_CHUNK_MAX_SIZE + 1, comparator);
    }

    // ****************************      Method implementations     *************************** //
    
    @Override
    public int size() {
        int size = 0;
        for (List<T> list : unsortedChunks) {
            size += list.size();
        }
        return super.size() + size;
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty() && size() == 0;
    }

    @Override
    public T peek() {
        checkEmptyHead();
        return super.peek(); 
    }    

    @Override
    public T poll() {
        checkEmptyHead();
        return super.poll(); 
    }    
    
    @Override
    public boolean offer(T e) {
        checkSplitOfChunk(addObject(e));
        return true;
    }
    
    /**
     * Adds given object to specified chunk of this collection and return the modified chunk index or -1 if it
     *  was added to the sorted top.
     * @param e object to be added
     * @return the modified chunk index or -1 if it was added to the sorted top.
     */
    private int addObject(T e) {
        if (unsortedChunks.isEmpty() && super.size() >= INITIAL_CHUNK_MAX_SIZE) {
            createNewChunk(0, Float.MAX_VALUE);
        }
        float eDist = e.getQueryDistance();
        if (unsortedChunks.isEmpty() || eDist < chunkBorders.get(0)) {
            super.offer(e);
            // if the unsortedChunks are empty, adjust the first chunk border
            if (eDist > chunkBorders.get(0)) {
                chunkBorders.set(0, e.getQueryDistance());
            }
            return -1;
        }
        int modifiedChunk = 0;
        int maxIndex = chunkBorders.size() - 1;
        do {
            int mid = (modifiedChunk + maxIndex) >>> 1;
            if (eDist < chunkBorders.get(mid)) {
                maxIndex = mid;
            } else if (eDist >= chunkBorders.get(mid + 1)) {
                modifiedChunk = mid + 1;
            } else {
                modifiedChunk = mid;
                break;
            }
        } while (true);

        unsortedChunks.get(modifiedChunk).add(e);
        return modifiedChunk;
    }
   

    // *****************************        Queue organisation    ****************************** //
    
    /**
     * If the head is empty, the next chunk should be reordered and stored in the top.
     */
    private void checkEmptyHead() {
        if (super.isEmpty() && (! unsortedChunks.isEmpty())) {
            ArrayList<T> firstUnsortedChunk;
            do {
                checkSplitOfChunk(0);
                float maxDistanceInTop = chunkBorders.get(0);
                for (T t : (firstUnsortedChunk = removeChunk(0))) {
                    super.offer(t);
                }
                chunkBorders.set(0, maxDistanceInTop);                
                
                // decrease the capacity of the new head of the unsorted chunks
                if (!unsortedChunks.isEmpty()) {
                    decreaseToCapacity(0);
                }
            } while (firstUnsortedChunk.isEmpty() && (! unsortedChunks.isEmpty()));
        }
    }
    
    /**
     * If the specified data chunk exceeds its maximal capacity, it is split into two chunks.
     * @param chunkIndex index of chunk to check
     */
    protected void checkSplitOfChunk(int chunkIndex) {
        // if the sorted head should be solved
        if (chunkIndex < 0) {
            return;
        } 
        if (unsortedChunks.get(chunkIndex).size() >= chunkCapacities.get(chunkIndex)) {
            ArrayList<T> currentChunk = unsortedChunks.get(chunkIndex);
            if ((size() > REDUCE_SIZE_LIMIT) && (unsortedChunks.get(unsortedChunks.size() - 1).size() < REDUCE_SIZE_LIMIT - MINIMUM_SIZE_LIMIT)) {
                // get rid of the last partition - simply remove it
                removeChunk(unsortedChunks.size() - 1);
                if (chunkIndex == unsortedChunks.size()) {
                    return;
                }
            }
            if (increaseToMaxCapacity(chunkIndex) && currentChunk.size() < chunkCapacities.get(chunkIndex)) {
                return;
            }
            // if splitting the last and only chunk, we have to sort it cut in half
            float newBorder = (chunkIndex == unsortedChunks.size() - 1) ? 
                    (chunkBorders.get(chunkIndex) + (chunkBorders.get(0) / 2f)) : 
                    ((chunkBorders.get(chunkIndex) + chunkBorders.get(chunkIndex + 1)) / 2f);
            ArrayList<T> newChunk = createNewChunk(chunkIndex + 1, newBorder);
//            chunkCapacities.set(chunkIndex, maxCapFor(chunkIndex));
            ArrayList<T> currentChunkNew = new ArrayList<>(chunkCapacities.get(chunkIndex) + 1);
            for (T t : currentChunk) {
                ((t.getQueryDistance() < newBorder) ? currentChunkNew : newChunk).add(t);
            }
            unsortedChunks.set(chunkIndex, currentChunkNew);
            
            // adopt lazy strategy - if the split was not successful, increase the partition sizes and continue
            if (newChunk.isEmpty()) {
                removeChunk(chunkIndex + 1);
                chunkCapacities.set(chunkIndex, currentChunkNew.size() + 1);
            } else {
                chunkCapacities.set(chunkIndex, maxCapFor(chunkIndex));
            }
            if (currentChunkNew.isEmpty()) {
                chunkCapacities.set(chunkIndex + 1, newChunk.size() + 1);
                removeChunk(chunkIndex);
            }
        }
    }
    
    /**
     * Creates a new chunk with given index and given value interval.
     * @param newChunkIndex index of the new chunk
     * @param newChunkLowerBorder lower border of the chunk value interval
     * @return 
     */
    private ArrayList<T> createNewChunk(int newChunkIndex, float newChunkLowerBorder) {
        // create a new chunk
        chunkCapacities.set(newChunkIndex, maxCapFor(newChunkIndex));
        ArrayList<T> firstChunk = new ArrayList<>(chunkCapacities.get(newChunkIndex) + 1);
        unsortedChunks.add(newChunkIndex, firstChunk);
        chunkBorders.set(newChunkIndex, newChunkLowerBorder);
        return firstChunk;
    }
    
    protected int maxCapFor(int chunkIndex) {
        return (int) (INITIAL_CHUNK_MAX_SIZE * Math.min(100d, Math.pow(INCREASE_FACTOR, chunkIndex)));
    }

    protected boolean increaseToMaxCapacity(int chunkIndex) {
        int newMax = maxCapFor(chunkIndex);
        if (chunkCapacities.get(chunkIndex) < newMax) {            
            chunkCapacities.set(chunkIndex, newMax);
            unsortedChunks.get(chunkIndex).ensureCapacity(newMax);
            return true;
        }
        return false;
    }

    protected boolean decreaseToCapacity(int chunkIndex) {
        int newMax = maxCapFor(chunkIndex);
        if (chunkCapacities.get(chunkIndex) > newMax) {            
            chunkCapacities.set(chunkIndex, newMax);
            return true;
        }
        return false;
    }
    
    private ArrayList<T> removeChunk(int index) {
        chunkBorders.remove(index);
        chunkCapacities.remove(index);
        return unsortedChunks.remove(index);
    }
        
    
    @Override
    public Iterator<T> iterator() {
        throw new UnsupportedOperationException("Operation 'iterator()' is not supported in PartiallySortedCollection");
    }
    
    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("Operation 'contains' is not supported in PartiallySortedCollection");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("Operation 'containsAll' is not supported in PartiallySortedCollection");
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Operation 'remove(object)' is not supported in PartiallySortedCollection");
    }    

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("Operation 'toArray' is not supported in PartiallySortedCollection");
    }

    @Override
    public <E> E[] toArray(E[] array) {
        throw new UnsupportedOperationException("Operation 'toArray' is not supported in PartiallySortedCollection");
    }

    
    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Operation 'removeAll' is not supported in PartiallySortedCollection");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Operation 'retainAll' is not supported in PartiallySortedCollection");
    }

}
